<div id="node-<?php print $node->nid; ?>" class="node<?php if ($sticky) { print ' sticky'; } ?><?php if(!$page) { print ' node-teaser'; } ?><?php if (!$status) { print ' node-unpublished'; } ?> clear-block">

<?php if ($page == 0): ?>
  <h2 class='title'><a href="<?php print $node_url ?>" title="<?php print $title ?>"><?php print $title ?></a></h2>
<?php endif; ?>

  <div class="meta">
  <?php if ($submitted) : ?>
    <div class="submitted">
      <?php print t('By ') . theme('username', $node) . t(' - Posted on ') . format_date($node->created, 'custom', "F jS, Y"); ?>
    </div> 
  <?php endif; ?>
  
  <?php if ($terms) : ?>
    <div class="terms">Tagged: <?php print $terms ?></div>
  <?php endif;?>
  </div>
  
  <?php print $picture ?>
  
  <div class="content">
    <?php print $content ?>
  </div>

  <?php if ($links) { ?>
    <div class='node-links'>
      <?php print $links; ?>
    </div>
  <?php } ?>

</div>
