<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="<?php print $language ?>" xml:lang="<?php print $language ?>">

<head>
  
  <!--  
    BlueFire theme designed and built by Derek Webb of http://CollectiveColors.com
    Please see me if you would like to have modifications made to your website.

    Email me at: derek(at)collectivecolors(dot)com
  //-->

  <title><?php print $head_title ?></title>
  <?php print $head ?>
  <?php print $styles ?>
  <?php print $scripts ?>
  
  
  <!--[if lt IE 7]>
  <script type="text/javascript"><?php /* Needed to avoid Flash of Unstyle Content in IE */ ?> </script>
  <script defer type="text/javascript" src="/themes/bluefire/pngfix.js"></script>
  <![endif]-->
  
  
</head>

<body>

  <!-- [ Header and logo area ] -->
  <div class='header-div'>
    
    <div class='header-left-div'>
      <?php if ($logo) { ?><a href="<?php print $base_path ?>" title="<?php print t('Home') ?>"><img src="<?php print $logo ?>" alt="<?php print t('Home') ?>" class='logo-img' /></a><?php } ?>
      
      <div class='site-info-div'>  
        <?php if ($site_name) { ?><h1 class='site-name'><a href="<?php print $base_path ?>" title="<?php print t('Home') ?>"><?php print $site_name ?></a></h1><?php } ?>
        <?php if ($site_slogan) { ?><div class='site-slogan'><?php print $site_slogan ?></div><?php } ?>
      </div>
    </div>
    
    <div class='header-right-div'>    
      
      <?php if($search_box) { ?>
        <div class='searchbox-div'>
          <?php print $search_box ?>
        </div>
      <?php } ?>
          
      <!-- [ header blocks are meant only for inline menus they are displayed beneath the searchbox ] -->
      <?php if($header) { ?>
        <div class='header-blocks-div'><?php print $header ?></div>
      <?php } ?>
      
    </div>
    
    <div class='clear'></div>
    
  </div>

  
  <!-- [ Primary and secondary links area ] -->
  <div class='links-div'>
  
    <!-- [ Check for primary links ] --> 
    <?php if(!empty($primary_links)) { ?>
      <div class='plinks-div'>
        <?php print theme('links', $primary_links, array('class' =>'links', 'id' => 'navlist')) ?>
      </div>
    <?php } ?>
    
    <!-- [ Check for secondary links ] -->
    <?php if(!empty($secondary_links)) { ?>
      <div class='slinks-div'>
        <?php print theme('links', $secondary_links, array('class' =>'links', 'id' => 'subnavlist')) ?>
      </div>
    <?php } ?>
    
  </div>
  
  
  <!-- [ Main content body area ] -->
  <!-- [ Check for sidebar - we handle the content section differently if there is none ] -->
  <?php if (!empty($sidebar)) { ?>
    <div class='main-div'>
      <div class='content-div'>
  <?php } else { ?>
    <div class='main-nosb-div'>
      <div class='content-nosb-div'>
  <?php } ?>
      
      <!-- [ Content mid div allows padding without disrupting the containing floating div (content) ] -->
      <div class='content-mid-div'>
      
        <?php if($mission) { ?><div class="mission-div"><?php print check_plain($mission) ?></div><?php } ?>
        <?php if($breadcrumb) { ?><div class='bc-div'><?php print $breadcrumb ?></div><?php } ?>
      
        <h1 class="title"><?php print $title ?></h1>
        <div class="tabs-div"><?php print $tabs ?></div>
      
        <?php print $help ?>
        <?php print $messages ?>
          
        <!-- [ Content inner div allows padding without disrupting the containing floating div (content) ] -->
        <div class='content-inner-div'>
        
          <!-- [ Begin Content ] -->
          <?php print $content; ?>
          <!-- [ End Content ] -->
        
          <?php if($body) { ?>
            <div class='body-blocks-div'>
              <?php print $body ?>
            </div>
          <?php } ?>
        
        </div>
      
        <?php print $feed_icons; ?>
      </div>
    </div>
        
    <!-- [ Sidebar left area - floats right too - ensures that content is first to process - SEO ] -->
    <?php if($sidebar) { ?>
      <div class='sidebar-div'>
        <!-- [ Sidebar inner div allows padding without disrupting the containing floating div (content) ] -->
        <div class='sidebar-inner-div'>
          <?php print $sidebar ?>
        </div>
      </div>
    <?php } ?>     
  
    <div class='clear'></div>
  
  </div>
  
  <div class='footer-div'>
    <?php print $footer_message ?>
  </div>
  <?php print $closure ?>
  
</body>
</html>

